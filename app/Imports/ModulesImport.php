<?php

namespace App\Imports;

use App\Module;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Validators\Failure;
use Throwable;
use Exception;
use App\Notifications\ImportHasFailedNotification;

class ModulesImport implements
    ToCollection,
    WithHeadingRow,
    SkipsOnError,
    WithValidation,
    WithChunkReading,
    ShouldQueue,
    WithEvents
{
    use Importable, SkipsErrors, RegistersEventListeners;



    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $module = Module::create([
                'module_code' => $row['module_code'],
                'module_name' => $row['module_name'],
                'module_term' => $row['module_term']
            ]);

        }
       
    }

    public function rules(): array
    {
       
        return [
            'module_code' => 'required|regex:/^[a-zA-Z]+$/u',
            '*.module_code' => 'required|regex:/^[a-zA-Z]+$/u',
            'module_name' => 'required',
            '*.module_name' => 'required',
            'module_term' => 'required',
            '*.module_term' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            
            $header_array = ['module_code','module_name','module_term'];
            foreach($validator->getData() as $row){
                foreach($row as $key=>$column){
                    if (count($row) > 3 ) {
                        throw new Exception('Header Column count exceeded,Column count should be 3');
                    }

                    if (!in_array($key,$header_array)) {
                        throw new Exception('Invalid Header Column Found, '.$key);
                    }
                }
                break;
            }

            if(count($validator->getData()) > 1000){
                throw new Exception('Row count Exceeded, Row count should be 1000');
            }

        });

       
    }

    public function registerEvents(): array
    {
        $mailto = 'charush@accubits.com';
        return [
            ImportFailed::class => function(ImportFailed $event) {
                $mailto->notify(new ImportHasFailedNotification);
            },
        ];
    }

   

    public function chunkSize(): int
    {
        return 1000;
    }

    public static function afterImport(AfterImport $event)
    {
    }

    public function onFailure(Failure ...$failure)
    {
    }

   


    
}