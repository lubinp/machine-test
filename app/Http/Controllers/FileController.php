<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\Imports\ModulesImport;
use App\Notifications\ImportHasFailedNotification;

class FileController extends Controller
{
    public function upload_csv(Request $request){

        $response = array();

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:csv,txt' 
        ]);

        if ($validator->fails())
        {
            $response['errors'] = $validator->errors();
            return response()->json($response);
        }

        $file = $request->file('file')->store('import');

        $import = new ModulesImport;

        try {
            $import->import($file);
            $response['success'] = 'Importing in queue, please wait...';
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            $response['errors'] = $failures;
        } catch (\Exception $ex) {
            $response['error'] = $ex->getMessage();
        } catch (\Error $er) {
            $response['error'] = $ex->getMessage();
        }
        
        return response()->json($response);
    }

}
